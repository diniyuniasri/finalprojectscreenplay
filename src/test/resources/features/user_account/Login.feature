@cucumber
@smoke
Feature: Sign in with an existing account
  As an online customer with a login account
  user should be able to sign in with account credentials
  to purchase new items
  and access their order history

  Scenario: Sign in with valid admin account
    Given that Dini is a registered member
    When she logs in with valid credentials
      | username                          | password |
      | dini.yuniasri@gmail.com           | OODinyun1221|
    Then he should be able to view his account profile
