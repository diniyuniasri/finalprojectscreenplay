@cucumber
@smoke
Feature: Order with an existing account
  As an online customer with a login account
  user should be able to sign in with account credentials
  to purchase new items
  and access their order history

  Scenario: Member order with default address
    Given that Dini is a registered member
    When she logs in with valid credentials
      | username                          | password    |
      | dini.yuniasri@gmail.com           | OODinyun1221|
    And she click the Homepage
#    When she click on product 6
    And she type on search field 'brown bear cushion'
    And she click '2' in 'Quantity'
    When she click Add to Cart
#      | Product               | Color | Quantity | Description |
#      | brown bear cushion    | Black |    1     | Cushion with removable cover and invisible zip on the back. 32x32cm |
    When she proceeds to the checkout
    When she click Proceed To Checkout in Shopping Cart
    When she proceeds to the checkout with default address
    And she click the Homepage

