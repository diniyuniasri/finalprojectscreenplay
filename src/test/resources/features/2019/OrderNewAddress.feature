@cucumber
@smoke
Feature: Order with an existing account
  As an online customer with a login account
  user should be able to sign in with account credentials
  to purchase new items
  and access their order history

  Scenario: Member order with new address
    Given that Dini is a registered member
    When she logs in with valid credentials
      | username                          | password    |
      | dini.yuniasri@gmail.com           | OODinyun1221|
    And she click the Homepage
    When she click on product 6
    When she click '2' in 'Quantity'
    When she click Add to Cart
    When she proceeds to the checkout
    When she click Proceed To Checkout in Shopping Cart
    When she click add new address:
      | firstname    | lastname   | company             | vat                      | address       | addressComplement  | zip   | city      | country   | state       | phone         |
      | Reza         | Pahlevi    | PT. Bukalapak.com   | 84.903.329.5-614.000     | Jl. Raya ITS  | PENS               | 60111 | Surabaya  | Indonesia | Jawa Timur  | 085721127495  |
    And she click the Homepage