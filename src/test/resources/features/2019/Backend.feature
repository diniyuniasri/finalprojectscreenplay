@cucumber
@smoke
Feature: Order with an existing account
  As an online customer with a login account
  user should be able to sign in with account credentials
  to purchase new items
  and access their order history

  Scenario: Member order with new address
    Given that Reza is a registered member on backend
    When he logs in with valid credentials in backend
      | username                          | password    |
      | zapahlevie@gmail.com              | levi1234    |
    And he click sub menu Orders
    And he click dropdown 'Awaiting bank wire payment' on Status
    And he find 'TEPMJMNLM' on Reference
    Then he click Search
    Then he click the result

    When he click status Payment Accepted
    Then he see 'Payment Accepted' in status
#    And she click the Homepage
#    When she click on product 6
#    When she click '2' in 'Quantity'
#    When she click Add to Cart
#    When she proceeds to the checkout
#    When she click Proceed To Checkout in Shopping Cart
#    When she click add new address:
#      | firstname    | lastname   | company             | vat                      | address       | addressComplement  | zip   | city      | country   | state       | phone         |
#      | Reza         | Pahlevi    | PT. Bukalapak.com   | 84.903.329.5-614.000     | Jl. Raya ITS  | PENS               | 60111 | Surabaya  | Indonesia | Jawa Timur  | 085721127495  |
