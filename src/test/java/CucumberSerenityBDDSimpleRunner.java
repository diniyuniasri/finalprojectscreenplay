import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features="src/test/resources/features/2019/OrderNewAddress.feature",
//@CucumberOptions(features="src/test/resources/features/2019/Backend.feature",
//@CucumberOptions(features="src/test/resources/features/user_account/Login.feature",
        plugin = {}, tags ={})
public class CucumberSerenityBDDSimpleRunner {
}