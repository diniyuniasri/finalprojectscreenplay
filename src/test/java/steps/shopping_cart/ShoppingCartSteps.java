package steps.shopping_cart;

import cucumber.api.DataTable;
import cucumber.api.PendingException;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import screenplay.abilities.Address;
//import screenplay.questions.CartSummary;
//import screenplay.questions.Checkout;
import screenplay.tasks.*;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsString;

public class ShoppingCartSteps {

    @Before
    public void set_the_stage(){
        OnStage.setTheStage(new OnlineCast());
    }

    @When("^(?:he|she|they) proceeds to the checkout$")
    public void he_proceeds_to_the_checkout() {
        theActorInTheSpotlight().attemptsTo(ProceedToCheckOut.fromQuickViewPopup());
    }

    @When("^(?:he|she|they) click on product (\\d+)$")
    public void sheClickOnProduct(int product) throws Throwable {
        theActorInTheSpotlight().attemptsTo(AddItemToCart.fromHomePage(product));
    }

    @When("^(?:he|she|they) click '(.*)' in '(.*)'$")
    public void iClickInQuantity(String input, String keyword) throws Throwable {
        switch (keyword) {
            case "Quantity":
                theActorInTheSpotlight().has(AddQuantitiy.with(input));
                break;
        }
    }

    @When("^(?:he|she|they) click Add to Cart$")
    public void sheClickAddToCart() throws Throwable {
        theActorInTheSpotlight().attemptsTo(AddItemToCart.clickAddToCart());
    }

    @When("^(?:he|she|they) click add new address:$")
    public void sheProceedsToTheCheckoutWithNewAddress(DataTable data) throws Throwable {
        theActorInTheSpotlight().attemptsTo(ProceedToCheckOut.clickAddAddress());
        theActorInTheSpotlight()
                .whoCan(Address.with(data))
                .attemptsTo(AddNewAddress.withNewAddress());
        theActorInTheSpotlight().attemptsTo(ProceedToCheckOut.clickAddressCheckbox());
    }

    @When("^(?:he|she|they) click Proceed To Checkout in Shopping Cart$")
    public void sheClickProceedToCheckoutInShoppingCart() throws Throwable {
        theActorInTheSpotlight().attemptsTo(ProceedToCheckOut.fromCartSummaryPopup());
    }

    @When("^(?:he|she|they) proceeds to the checkout with default address$")
    public void heSheTheyProceedsToTheCheckoutWithDefaultAddress() throws Throwable {
        theActorInTheSpotlight().attemptsTo(ProceedToCheckOut.fromCheckoutSummary());
    }

    @And("^(?:he|she|they) type on search field '(.*)'$")
    public void sheTypeOnSearchFieldBrownBearCushion(String input) throws Throwable {
        theActorInTheSpotlight().has(SearchAnItem.with(input));
        theActorInTheSpotlight().attemptsTo(AddItemToCart.fromSearchResultsPage());
    }
}
