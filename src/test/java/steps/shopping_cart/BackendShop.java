package steps.shopping_cart;

import cucumber.api.DataTable;
import cucumber.api.PendingException;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import screenplay.abilities.Address;
import screenplay.tasks.*;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsString;

public class BackendShop {

    @Before
    public void set_the_stage(){
        OnStage.setTheStage(new OnlineCast());
    }

    @And("^(?:he|she|they) click sub menu Orders$")
    public void heClickSubMenuOrders() throws Throwable {
        theActorInTheSpotlight().attemptsTo(ChangeStatusBackend.subMenuOrder());
    }

    @And("^(?:he|she|they) click dropdown '(.*)' on Status$")
    public void heClickDropdownOnStatus(String keyword) throws Throwable {
        switch (keyword) {
            case "Awaiting bank wire payment":
                theActorInTheSpotlight().attemptsTo(ChangeStatusBackend.findAwaitingStatus());
                break;
            case "Payment accepted":
                theActorInTheSpotlight().attemptsTo(ChangeStatusBackend.findAcceptPaymentStatus());
                break;
        }
    }

    @And("^(?:he|she|they) find '(.*)' on Reference$")
    public void heFindOnReference(String input) throws Throwable {
        theActorInTheSpotlight().has(BackendReference.with(input));
    }

    @Then("^(?:he|she|they) click Search$")
    public void heClickSearch() throws Throwable {
        theActorInTheSpotlight().has(ChangeStatusBackend.clickSearch());
    }

    @Then("^(?:he|she|they) click the result$")
    public void heClickTheResult() throws Throwable {
        theActorInTheSpotlight().has(ChangeStatusBackend.dataTable());
    }

    @When("^(?:he|she|they) click status Payment Accepted$")
    public void heClickStatusPaymentAccepted() throws Throwable {
        theActorInTheSpotlight().has(ChangeStatusBackend.dropDownstatus());
    }

    @Then("^(?:he|she|they) see '(.*)' in status$")
    public void heSeeTheStatusIsPaymentAccepted(String keyword) throws Throwable {
//        theActorInTheSpotlight().has(ChangeStatusBackend.seeStatusNow(keyword));
    }

    @Then("^he do something$")
    public void heDoSomething() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }
}
