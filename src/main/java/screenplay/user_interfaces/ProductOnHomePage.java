package screenplay.user_interfaces;

import net.serenitybdd.screenplay.targets.Target;

public class ProductOnHomePage {

    public static Target product = Target.the("Product")
            .locatedBy("//*[@id=\"content\"]/section/div/article[6]/div/div[1]/h3/a");

    public static Target Quantity = Target.the("Quantity bar")
            .locatedBy("//*[@id=\"quantity_wanted\"]");
}
