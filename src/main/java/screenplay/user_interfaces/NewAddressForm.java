package screenplay.user_interfaces;

import net.serenitybdd.screenplay.targets.Target;

public class NewAddressForm {

    public static Target FILL_FIRSTNAME = Target.the("firstname field")
            .locatedBy("//*[@id=\"delivery-address\"]/div/section/div[1]/div[1]/input");
    public static Target FILL_LASTNAME = Target.the("lastname field")
            .locatedBy("//*[@id=\"delivery-address\"]/div/section/div[2]/div[1]/input");
    public static Target FILL_COMPANY = Target.the("company field")
            .locatedBy("//*[@id=\"delivery-address\"]/div/section/div[3]/div[1]/input");
    public static Target FILL_VAT = Target.the("vat field")
            .locatedBy("//*[@id=\"delivery-address\"]/div/section/div[4]/div[1]/input");
    public static Target FILL_ADDRESS = Target.the("address field")
            .locatedBy("//*[@id=\"delivery-address\"]/div/section/div[5]/div[1]/input");
    public static Target FILL_ADDRESSCOMPLEMENT = Target.the("address complement field")
            .locatedBy("//*[@id=\"delivery-address\"]/div/section/div[6]/div[1]/input");
    public static Target FILL_ZIP = Target.the("zip field")
            .locatedBy("//*[@id=\"delivery-address\"]/div/section/div[7]/div[1]/input");
    public static Target FILL_CITY = Target.the("city field")
            .locatedBy("//*[@id=\"delivery-address\"]/div/section/div[8]/div[1]/input");
    public static Target FILL_PHONE = Target.the("phone field")
            .locatedBy("//*[@id=\"delivery-address\"]/div/section/div[11]/div[1]/input");

    //choose from dropdown
    //state = "Jawa Timur"
    public static Target CHOOSESTATE = Target.the("state field")
            .locatedBy("//*[@id=\"delivery-address\"]/div/section/div[9]/div[1]/select/option[text()=\"Jawa Timur\"]");
    //country = "Indonesia"
    public static Target CHOOSECOUNTRY = Target.the("state field")
            .locatedBy("//*[@id=\"delivery-address\"]/div/section/div[10]/div[1]/select/option[text()=\"Indonesia\"]");
}
