package screenplay.user_interfaces;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;

public class GlobalMenu {

    public static Target click_homepage = Target.the("Main Page")
            .locatedBy("//*[@id=\"_desktop_logo\"]");

}
