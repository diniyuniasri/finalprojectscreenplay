package screenplay.user_interfaces;

import net.serenitybdd.screenplay.targets.Target;

public class UIBackend {

    public static Target menuOrder = Target.the("menu Order Backend")
            .locatedBy("//*[@id=\"subtab-AdminParentOrders\"]");
    public static Target submenuOrder = Target.the("submenu Order Backend")
            .locatedBy("//*[@id=\"subtab-AdminOrders\"]");

    public static Target dropAwaitingStatus = Target.the("click dropdown Awaiting Status")
            .locatedBy("//*[@id=\"table-order\"]/thead/tr[2]/th[9]/]/select/option[2]");
    public static Target dropAcceptPaymentStatus = Target.the("click dropdown Delivered Status")
            .locatedBy("//*[@id=\"table-order\"]/thead/tr[2]/th[9]/]/select/option[9]");

    public static Target inputReference = Target.the("find something on reference")
            .locatedBy("//*[@id=\"table-order\"]/thead/tr[2]/th[3]/input");

    public static Target searchbutton = Target.the("search button to search")
            .locatedBy("//*[@id=\"submitFilterButtonorder\"]");

    public static Target dataResult = Target.the("result of data table")
            .locatedBy("//*[@id=\"table-order\"]/tbody/tr[1]");

    public static Target updateStatus = Target.the("change status")
            .locatedBy("//*[@id=\"id_order_state_chosen\"]/a");
    public static Target btnUpdateStatus = Target.the("button update status")
            .locatedBy("//*[@id=\"submit_state\"]");
    public static Target rowTable = Target.the("row table")
            .locatedBy("//*[@id=\"status\"]/div/table/tbody/tr[1]/td[2]");
}
