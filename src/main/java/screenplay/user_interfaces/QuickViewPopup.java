package screenplay.user_interfaces;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;

public class QuickViewPopup {

    public static Target ADD_ITEM_TO_CART = Target.the("Add item to cart")
            .locatedBy("#add-to-cart-or-refresh > div.product-add-to-cart > div > div.add > button");
    public static Target PROCEED_TO_CHECKOUT = Target.the("Proceed to checkout")
            .locatedBy("//*[@id=\"blockcart-modal\"]/div/div/div[2]/div/div[2]/div/div/a");
}
