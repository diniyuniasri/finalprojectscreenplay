package screenplay.abilities;

import cucumber.api.DataTable;
import net.serenitybdd.screenplay.Ability;
import net.serenitybdd.screenplay.Actor;
import screenplay.exceptions.CannotAuthenticateException;
import utils.ConvertCucumberDataTable;

import java.util.Map;

public class Address implements Ability {
    private final String firstname, lastname, company, vat, address, addressComplement, zip, city, country, state, phone;

    // instantiates the Ability and enables fluent DSL
    public static Address with(String firstname, String lastname, String company, String vat, String address, String addressComplement,
                               String zip, String city, String country, String state, String phone){
        return new Address(firstname, lastname, company, vat, address, addressComplement, zip, city, country, state, phone);
    }
    public static Address with(DataTable credentialsData){
        Map<String,String> credentials =  ConvertCucumberDataTable.toMap(credentialsData);
        return new Address(credentials.get("firstname"), credentials.get("lastname"), credentials.get("company"), credentials.get("vat"),
                credentials.get("address"), credentials.get("addressComplement"), credentials.get("zip"), credentials.get("city"),
                credentials.get("country"), credentials.get("state"), credentials.get("phone"));
    }

    public static Address as(Actor actor) throws CannotAuthenticateException {
        return actor.abilityTo(Address.class);
    }

    public String firstname() {
        return firstname;
    }

    public String lastname() {
        return lastname;
    }

    public String company() {
        return company;
    }

    public String vat() {
        return vat;
    }

    public String address() {
        return address;
    }

    public String addressComplement() {
        return addressComplement;
    }

    public String zip() {
        return zip;
    }

    public String city() {
        return city;
    }

    public String country() {
        return country;
    }

    public String state() {
        return state;
    }

    public String phone() {
        return phone;
    }


    private Address(String firstname, String lastname, String company, String vat, String address, String addressComplement, String zip, String city, String country, String state, String phone) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.company = company;
        this.vat = vat;
        this.address = address;
        this.addressComplement = addressComplement;
        this.zip = zip;
        this.city = city;
        this.country = country;
        this.state = state;
        this.phone = phone;
    }
}
