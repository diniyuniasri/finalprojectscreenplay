package screenplay.tasks;

import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.conditions.Check;
import screenplay.user_interfaces.UIBackend;

import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.containsText;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.hasValue;
import static net.serenitybdd.screenplay.questions.WebElementQuestion.valueOf;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class ChangeStatusBackend {
    public static Task subMenuOrder(){
        return Task.where("click the sub menu order",
                Check.whether(valueOf(UIBackend.menuOrder), isVisible())
                        .andIfSo(Click.on(UIBackend.menuOrder)),
                Check.whether(valueOf(UIBackend.submenuOrder), isVisible())
                        .andIfSo(Click.on(UIBackend.submenuOrder))
        );
    }

    public static Task findAwaitingStatus(){
        return Task.where("find the Awaiting bank wire payment status",
                Check.whether(valueOf(UIBackend.dropAwaitingStatus), isVisible())
                        .andIfSo(Click.on(UIBackend.dropAwaitingStatus))
        );
    }

    public static Task findAcceptPaymentStatus(){
        return Task.where("find the Accept payment status",
                Check.whether(valueOf(UIBackend.dropAcceptPaymentStatus), isVisible())
                        .andIfSo(Click.on(UIBackend.dropAcceptPaymentStatus))
        );
    }

    public static Task clickSearch(){
        return Task.where("click search", Click.on(UIBackend.searchbutton));
    }

    public static Task dataTable(){
        return Task.where("click the first row of the result",
                Check.whether(valueOf(UIBackend.dataResult), isVisible())
                        .andIfSo(Click.on(UIBackend.dataResult))
        );
    }

    public static Task dropDownstatus(){
        return Task.where("change the status",
                Click.on(UIBackend.updateStatus),
                Click.on(UIBackend.btnUpdateStatus)
        );
    }

    public static Task seeStatusNow(String keyword){
        return Task.where("see the status of the customer",
                Check.whether(valueOf(UIBackend.rowTable), hasValue("Awaiting bank wire payment"))
        );
    }
}
