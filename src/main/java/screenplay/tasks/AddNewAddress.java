package screenplay.tasks;

import cucumber.api.DataTable;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.thucydides.core.annotations.Step;
import screenplay.abilities.Address;
import screenplay.abilities.Authenticate;

import static net.serenitybdd.screenplay.Tasks.instrumented;
import static screenplay.user_interfaces.LoginForm.SIGN_IN;
import static screenplay.user_interfaces.NewAddressForm.*;


public class AddNewAddress implements Task {

//    DataTable data;

    @Step
    public static AddNewAddress withNewAddress(){
        return instrumented(AddNewAddress.class);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
//        actor.whoCan(Address.with(dataAddress))
        actor.attemptsTo(
                Enter.theValue(checkAddress(actor).firstname())
                        .into(FILL_FIRSTNAME),
                Enter.theValue(checkAddress(actor).lastname())
                        .into(FILL_LASTNAME),
                Enter.theValue(checkAddress(actor).company())
                        .into(FILL_COMPANY),
                Enter.theValue(checkAddress(actor).vat())
                        .into(FILL_VAT),
                Enter.theValue(checkAddress(actor).address())
                        .into(FILL_ADDRESS),
                Enter.theValue(checkAddress(actor).addressComplement())
                        .into(FILL_ADDRESSCOMPLEMENT),
                Enter.theValue(checkAddress(actor).zip())
                        .into(FILL_ZIP),
                Enter.theValue(checkAddress(actor).city())
                        .into(FILL_CITY),
                Click.on(CHOOSESTATE),
                Click.on(CHOOSECOUNTRY),
                Enter.theValue(checkAddress(actor).phone())
                        .into(FILL_PHONE)
//                Click.on(SIGN_IN)
        );
    }

    private Address checkAddress(Actor actor) {
        return Address.as(actor);
    }

}
