package screenplay.tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.thucydides.core.annotations.Step;
import screenplay.abilities.Authenticate;

import static net.serenitybdd.screenplay.Tasks.instrumented;
import static screenplay.user_interfaces.LoginForm.*;

public class LogInBackend implements Task {

    @Step("Logs in loaded {0}")
    public static LogInBackend withCredentials(){
        return instrumented(LogInBackend.class);
    }

    @Step
    public <T extends Actor> void performAs(T user) {
        user.attemptsTo(
                Enter.theValue(authenticated(user).username())
                    .into(username_backend),
                Enter.theValue(authenticated(user).password())
                    .into(password_backend),
                Click.on(sign_in_backend)
        );
    }

    private Authenticate authenticated(Actor actor) {
       return Authenticate.as(actor);
    }

}
