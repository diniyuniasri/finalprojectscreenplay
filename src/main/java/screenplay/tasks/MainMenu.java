package screenplay.tasks;

import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.conditions.Check;
import screenplay.user_interfaces.GlobalMenu;

import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;
import static net.serenitybdd.screenplay.questions.WebElementQuestion.valueOf;

public class MainMenu {

    public static Task clickMenu(){
        return Task.where("Click the Header",
                Check.whether(
                        valueOf(GlobalMenu.click_homepage),isVisible())
                        .andIfSo(Click.on(GlobalMenu.click_homepage)));
    }

}
