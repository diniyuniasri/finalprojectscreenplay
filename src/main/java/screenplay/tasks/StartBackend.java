package screenplay.tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Open;
import net.serenitybdd.screenplay.conditions.Check;
import net.thucydides.core.annotations.Step;
import screenplay.actions.Refresh;
import screenplay.user_interfaces.ApplicationHomePage;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class StartBackend implements Task {

    private Performable steps;
    private String title;

    private ApplicationHomePage applicationHomePage;

    @Step("{0} starts and performs the step **#title**")
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Open.browserOn().the(applicationHomePage),
                Refresh.theBrowserSession()
//               ,Check.whether(steps != null).andIfSo(Task.where(title, steps))
        );
    }

    public static StartBackend prepareToSignIn() {
        return instrumented(StartBackend.class, "Go to Login Screen", GoToLogin.called());
    }

    public static StartBackend readyToSearch() {
        return instrumented(StartBackend.class, "User ready to Search", null);
    }

    public StartBackend(String title, Performable steps) {
        this.title = title;
        this.steps = steps;
    }
}
