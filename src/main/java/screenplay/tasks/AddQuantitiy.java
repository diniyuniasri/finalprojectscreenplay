package screenplay.tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Enter;
import org.openqa.selenium.Keys;

import static net.serenitybdd.screenplay.Tasks.instrumented;
import static screenplay.user_interfaces.ProductOnHomePage.Quantity;

public class AddQuantitiy implements Task {
    private String keyword;

    public static AddQuantitiy with(String keyword){
        return instrumented(AddQuantitiy.class, keyword);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Enter.theValue(keyword)
                        .into(Quantity)
        );
    }

    public AddQuantitiy(String keyword) {
        this.keyword = keyword;
    }
}
