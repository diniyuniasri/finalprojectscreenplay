package screenplay.tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Enter;
import org.openqa.selenium.Keys;

import static net.serenitybdd.screenplay.Tasks.instrumented;
import static screenplay.user_interfaces.UIBackend.inputReference;

public class BackendReference implements Task {

    private String keyword;

    public static BackendReference with(String keyword){
        return instrumented(BackendReference.class, keyword);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Enter.theValue(keyword)
                        .into(inputReference)
                        .thenHit(Keys.ENTER)
        );
    }

    public BackendReference(String keyword) {
        this.keyword = keyword;
    }

}
